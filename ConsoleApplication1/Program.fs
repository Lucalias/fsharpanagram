﻿module Program

open System

[<EntryPoint>]
let main (args : string[]) = 
    let list = Anagram.mostAnagrams
    printfn "%A" list
    ignore(Console.ReadKey())
    0